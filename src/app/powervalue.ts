export interface Powervalue {
    id?: number | null;
    name?: string | null;
    adddate?: Date | null;
    v12v?: number | null;
    v23v?: number | null;
    v31v?: number | null;
    i1a?: number | null;
    i2a?: number | null;
    i3a?: number | null;
    ptkw?: number | null;
    pft?: number | null;
    frqhz?: number | null;
    ealmport?: number | null;
    rateaa?: number | null;
    counteraa?: number | null;
    gastempaa?: number | null;
    pressureaa?: number | null;
    velocityaa?: number | null;

}
