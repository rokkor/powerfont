import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { Observable, map } from 'rxjs';
import { SaveconfigService } from 'saveconfig.service';
import { environment } from 'src/environments/environment';
import { Powervalue } from '../powervalue';
import { PowervalueService } from '../powervalue.service';

@Component({
  selector: 'app-powerlist',
  templateUrl: './powerlist.component.html',
  styleUrls: ['./powerlist.component.css']
})
export class PowerlistComponent implements OnInit {
  sdate: any
  edate: any
  file: FileList | null = null;
  rows: Powervalue[] = []
  constructor(private ps: PowervalueService, private http: HttpClient, private ss: SaveconfigService, private bar: MatSnackBar) { }

  setfile(e: any) {
    this.file = e.target.files
    console.log('file', this.file)
  }
  show() {
    this.ps.sn(this.sdate, this.edate).subscribe(d => {
      console.log('Found ', d)
      this.rows = d as any
      this.bar.open('Found ', this.rows.length + "", { duration: 2000 })
    })

    this.ss.save('listpower', { s: this.sdate, e: this.edate })
  }
  upload() {
    let URL = environment.serverurl + '/upload'
    // let fileList: FileList = this.file;
    if (this.file!!.length > 0) {
      let f: File = this.file!![0];
      let formData: FormData = new FormData();
      formData.append('afile', f, f.name);
      // formData.append('filename',f.name,f.name)
      let headers = new Headers();
      //  headers.append('Content-Type', 'multipart/form-data; boundary=HereGoes');
      this.http.post(URL, formData).subscribe(d => {
        console.log('Upload ok')
        this.bar.open('Upload ',d+"(s)", { duration: 2000 })
      })
    }
    // this.service.http.post(URL, formData)
  }
  export()
  {
    let url = environment.serverurl + '/exporttoexcel'
     this.http.post(url, { sdate: this.sdate, edate: this.edate }, { responseType: "blob" }).subscribe(d=>{
      let s = moment(this.sdate)
      let e = moment(this.edate)
      let n = s.format("YYYY-MM-DD") + '_' + e.format("YYYY-MM-DD")
      FileSaver.saveAs(d, n + "-exports.xlsx");
      this.bar.open('Export ', 'Ok', { duration: 2000 })
    }, e => {
      this.bar.open("Export error", e, { duration: 5000 })
    })
  }
  delete() {
    this.ps.deletebydate(this.sdate, this.edate).subscribe(d => {
      console.log('Delete', d)
      this.bar.open('Delete ','Ok',{duration:2000})
    })
  }
  ngOnInit(): void {
    let o = this.ss.get('listpower')
    if (o != null) {
      this.sdate = o.s
      this.edate = o.e
    }
  }

}
