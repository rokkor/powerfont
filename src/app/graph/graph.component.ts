import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartConfiguration, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { PowervalueService } from '../powervalue.service';
import { default as Annotation } from 'chartjs-plugin-annotation';
import { SaveconfigService } from 'saveconfig.service';
import * as moment from 'moment';
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  sdate: any
  edate: any
  lables = []
  public lineChartData: ChartConfiguration['data'] = {
    datasets: [
      {
        data: [],
        label: 'pressureaa',
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)',
        fill: 'origin',
      },
      {
        data: [],
        label: 'gastempaa',
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      },
      {
        data: [],
        label: 'frqhz',
        backgroundColor: '#D5C9C6',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#C6D5CB',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      },
      {
        data: [],
        label: 'v12v',
        backgroundColor: '#2FD566',
        borderColor: '#829889',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#C6D5CB',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      },
      {
        data: [],
        label: 'v23v',
        backgroundColor: '#CAEA17',
        borderColor: '#62683D',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#C6D5CB',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      },
      {
        data: [],
        label: 'v31v',
        backgroundColor: '#1867C2',
        borderColor: '#3D5168',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#C6D5CB',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      },
    ],
    labels: this.lables
  };

  public lineChartOptions: ChartConfiguration['options'] = {
    elements: {
      line: {
        tension: 0.5
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      x: {},
      'y-axis-0':
      {
        position: 'left',
      },
      'y-axis-1': {
        position: 'right',
        grid: {
          color: 'rgba(255,0,0,0.3)',
        },
        ticks: {
          color: 'red'
        }
      }
    },

    plugins: {
      legend: { display: true },
      annotation: {
        annotations: [
          {
            type: 'line',
            scaleID: 'x',
            value: 'March',
            borderColor: 'orange',
            borderWidth: 2,
            label: {
              position: 'center',
              enabled: true,
              color: 'orange',
              content: 'LineAnno',
              font: {
                weight: 'bold'
              }
            }
          },
        ],
      }
    }
  };

  public lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;
  constructor(private ps:PowervalueService,private ss:SaveconfigService) { }

  ngOnInit(): void {
    let o = this.ss.get('g')
    if(o!=null)
    {
      this.sdate = o.s
      this.edate = o.e
    }
  }

  show()
  {
    this.ss.save('g',{s:this.sdate,e:this.edate})
    this.ps.sn(this.sdate,this.edate).subscribe((d:any)=>{
      console.log('Database ',d)
      this.lineChartData.labels!!.length=0
      this.lineChartData.datasets[0].data.length=0
      this.lineChartData.datasets[1].data.length=0
      this.lineChartData.datasets[2].data.length=0
      this.lineChartData.datasets[3].data.length=0
      this.lineChartData.datasets[4].data.length=0
      this.lineChartData.datasets[5].data.length=0
        d.map((i:any)=>{
          let time = i.adddate?.split('T')[1].split('.')[0]
          this.lineChartData.labels?.push(time)
          this.lineChartData.datasets[0].data.push(i.pressureaa)
          this.lineChartData.datasets[1].data.push(i.gastempaa)
          this.lineChartData.datasets[2].data.push(i.frqhz)
          this.lineChartData.datasets[3].data.push(i.v12v)
          this.lineChartData.datasets[4].data.push(i.v23v)
          this.lineChartData.datasets[5].data.push(i.v31v)
          
          
          
        })
        console.log('Datas ',this.lineChartData)
        this.chart?.update()
    })
  }
}
