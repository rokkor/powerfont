import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PowervalueService {

  constructor(private http: HttpClient) {

  }

  sn(s:any, e:any) {

    return this.http.post(environment.serverurl + "/list", { sdate: s, edate: e })
  }

  deletebydate(s:any,e:any)
  {
    return this.http.post(environment.serverurl+"/deletebydate",{sdate:s,edate:e})
  }
}
