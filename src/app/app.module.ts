import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PowervalueService } from './powervalue.service';
import { HttpClientModule } from '@angular/common/http';
import { PowerlistComponent } from './powerlist/powerlist.component';
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgChartsModule } from 'ng2-charts';
import {
  MatDatepickerModule,
  MatMomentDateModule,
} from "@coachcare/datepicker";
import { SaveconfigService } from 'saveconfig.service';
import { GraphComponent } from './graph/graph.component';
@NgModule({
  declarations: [
    AppComponent,
    PowerlistComponent,
    GraphComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,FormsModule,
    MatDatepickerModule, MatMomentDateModule,

    NgChartsModule,

    MatIconModule,MatButtonModule,MatMenuModule,MatFormFieldModule,MatInputModule,MatSnackBarModule
  ],
  providers: [PowervalueService,SaveconfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
