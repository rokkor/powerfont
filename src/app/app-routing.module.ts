import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GraphComponent } from './graph/graph.component';
import { PowerlistComponent } from './powerlist/powerlist.component';

const routes: Routes = [
  { path: 'powerlist', component: PowerlistComponent },
  { path: 'graph', component: GraphComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
