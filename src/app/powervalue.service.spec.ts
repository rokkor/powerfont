import { TestBed } from '@angular/core/testing';

import { PowervalueService } from './powervalue.service';

describe('PowervalueService', () => {
  let service: PowervalueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PowervalueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
